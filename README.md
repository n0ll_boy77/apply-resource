# Apply Resources

Table of contents
------------

- [Sites](#sites)


### Sites

Link | Status | Positions | Mainly in | Detail
:-- | :--: | :--: | :--: | :--:
[DFG](https://www.dfg.de/en) | :heavy_check_mark:	| Research funding <br /> Funded Projects | Germany | -
[DAAD](https://www.daad.de/en) | :heavy_check_mark:	| Master <br/> Phd <br/> Research <br/> Scholarship <br/> Ranking <br/> International Programs | Germany | -
[Humboldt Foundation](https://www.humboldt-foundation.de/en/) | :heavy_check_mark:	| Sponsorship Research | Germany | -
[AIF](https://www.aif.de/english/home.html) | :heavy_check_mark:	| Funding Agencies | Germany | -
[German Research Institutions](https://www.gerit.org/en/) | :heavy_check_mark:	| Master <br/> Phd <br/> Research Subject | Germany | -
[Higher Education Compass](https://www.hochschulkompass.de/en/study-in-germany.html) | :heavy_check_mark:	| Master <br/> Phd <br/> Research Subject <br/> Programs | Germany | -
[ListServ](https://listserv.acm.org/) | :heavy_check_mark:	| Anything related to academy | Global | -
[EURAXESS](https://euraxess.ec.europa.eu/) | :heavy_check_mark:	| Phd <br/> Jobs | Europe | -
[Studyprogrammes](https://studyprogrammes.ch/) | :heavy_check_mark:	| Master <br/> Phd <br/> Research | Switzerland | -
[Study in Switzerland](https://www.studyinswitzerland.plus/) | :heavy_check_mark:	| Master <br/> Phd <br/> Research | Switzerland | Same site for the above link
[Chevening](https://www.chevening.org/) | :heavy_check_mark:	| Fund for england | England | -
[Position in UK](https://www.jobs.ac.uk/) | :heavy_check_mark:	| Master <br/> Phd <br/> Jobs | England | -
[Australia Awards Scholarships](https://www.dfat.gov.au/people-to-people/australia-awards/australia-awards-scholarships) | :heavy_check_mark:	| Scholarships | Australia | -
[Australian Government Research Training Program (AGRTP) International Fee Offset Scholarship](https://www.anu.edu.au/study/scholarships/find-a-scholarship/australian-government-research-training-program-agrtp) | :heavy_check_mark:	| Scholarships | Australia | -
[Study in Sweden](https://studyinsweden.se/) | :heavy_check_mark:	| Programmes <br/> Scholarships <br/> Phd <br/> Master | Sweden | -
[University Admissions](https://www.universityadmissions.se/intl/start) | :heavy_check_mark:	| Programmes <br/> Phd <br/> Master | Sweden | -
[EIFFEL SCHOLARSHIP PROGRAM OF EXCELLENCE](https://www.campusfrance.org/en/eiffel-scholarship-program-of-excellence) | :heavy_check_mark:	| Scholarships | France | -
[Ecole normale supérieure](https://www.ens.psl.eu/en/academics/admissions/international-selection) | :heavy_check_mark:	| Scholarships | France | -
[Ampère Scholarships](http://www.ens-lyon.fr/en/studies/student-information/grants-and-scholarships#scholarships) | :heavy_check_mark:	| Scholarships | France | -
[ÉMILE BOUTMY SCHOLARSHIP](https://www.sciencespo.fr/students/en/fees-funding/financial-aid/emile-boutmy-scholarship) | :heavy_check_mark:	| Scholarships | France | -
[BOURSES INTERNATIONALES DE MASTER](https://www.universite-paris-saclay.fr/admission/bourses-et-aides-financieres/bourses-internationales-de-master) | :heavy_check_mark:	| Scholarships | France | -
[INSEAD MBA](https://www.insead.edu/master-programmes/mba/financing) | :heavy_check_mark:	| Scholarships | France | -
[CROUS & CNOUS](https://international.lescrous.fr/scholarships/) | :heavy_check_mark:	| Scholarships | France | -
[AUF](https://www.auf.org/) | :heavy_check_mark:	| Scholarships | France | -
[Enseignement supérieur](https://www.enseignementsup-recherche.gouv.fr/fr/enseignement-superieur-50387) | :heavy_check_mark:	| Scholarships | France | -
[AEFE](https://www.aefe.fr/aefe/operateur-du-ministere-de-leurope-et-des-affaires-etrangeres/dispositif-excellence-major) | :heavy_check_mark:	| Scholarships | France | -
[NSERC](https://www.nserc-crsng.gc.ca/students-etudiants/pg-cs/index_eng.asp) | :heavy_check_mark:	| Scholarships | Canada | -
[IDRC](https://www.idrc.ca/en/about-idrc) | :heavy_check_mark:	| Scholarships | Canada | -
[Awards Search Engine(outil)](http://www.outil.ost.uqam.ca/CRSH/) | :heavy_check_mark:	| Find Awards | Canada | -
[Canadian Research Information System](https://webapps.cihr-irsc.gc.ca/funding/Search?p_language=E&p_version=CIHR) | :heavy_check_mark:	| Find Awards | Canada | -
[Scholarships Canada](https://www.scholarshipscanada.com/) | :heavy_check_mark:	| Find Scholarships | Canada | -
[FULBRIGHT](https://foreign.fulbrightonline.org/) | :heavy_check_mark:	| Scholarships | USA | -
[Studielink](https://www.studielink.nl/) | :heavy_check_mark:	| ? | Netherlands | -
[Academic Transfer](https://www.academictransfer.com/en/) | :heavy_check_mark:	| Resarch Position <br/> Phd | Netherlands | -
[EURES](https://ec.europa.eu/eures/public/index_en) | :heavy_check_mark:	| Job Position | Europe | -
[Study in Holland](https://www.studyinholland.nl/) | :heavy_check_mark:	| Programmes <br/> Phd <br/> Master | Netherlands | -
[Scholarship DB](https://scholarshipdb.net/) | :heavy_check_mark:	| Programmes <br/> Phd <br/> Master <br/> Find Scholarships | Europe | -
[Grants](https://grants.at/en) | :heavy_check_mark:	| Find Scholarships <br/> Grants | Austria | -
[OEAD](https://oead.at/en/) | :heavy_check_mark:	| ? | Austria | -
[Learn in Austria](https://lerneninoesterreich.at/en/home#) | :heavy_check_mark:	| Phd <br/> Master <br/> Find position | Austria | -
[My German University](https://www.mygermanuniversity.com/) | :heavy_check_mark:	| Phd <br/> Master <br/> Find position | Germany | -
[Study in Germany](https://www.mygermanuniversity.com/) | :heavy_check_mark:	| Phd <br/> Master <br/> Find position | Germany | -
[Study Check](https://www.studycheck.de/) | :heavy_check_mark:	| Phd <br/> Master <br/> Find position | Germany | -
[Erasmus](https://erasmus-plus.ec.europa.eu/opportunities/opportunities-for-individuals/students/erasmus-mundus-joint-masters-scholarships) | :heavy_check_mark:	| Scholarships | Europe | -
[Shiksha Study Abroad](https://studyabroad.shiksha.com/) | :heavy_check_mark:	| Get Information | Global | Very good site for getting information from most universities in the world.
